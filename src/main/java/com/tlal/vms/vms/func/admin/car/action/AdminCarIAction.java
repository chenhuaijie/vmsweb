package com.tlal.vms.vms.func.admin.car.action;

/**
 * 管理员-汽车管理Action接口
 * @author chenhuaijie
 *
 */
public interface AdminCarIAction {
	/**
	 * 列表主页面
	 * @return
	 */
	public String goMain();
}
