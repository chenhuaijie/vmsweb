package com.tlal.vms.vms.func.admin.car.action;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.tlal.vms.base.action.BaseAction;
import com.tlal.vms.vms.func.admin.car.handler.AdminCarIHandler;

/**
 * 管理员-汽车逻辑控制器
 * 
 * @author Administrator 我会很好的
 */
@Namespace("/admin/car")
public class AdminCarAction extends BaseAction implements AdminCarIAction{
	private static final long serialVersionUID = 1877523299211116686L;

	@Resource
	private AdminCarIHandler adminCarHandler;

	@Override
	public Object getModel() {
		return adminCarHandler.getModel();
	}

	/**
	 * 列表显示
	 * 
	 * @return
	 */
	@Override
	@Action(value = "goMain", results = { @Result(location = "/pages/admin/car/AdminCarMain.jsp") })
	public String goMain() {
		return adminCarHandler.goMain();
	}
}
